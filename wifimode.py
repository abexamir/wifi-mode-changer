import argparse
import subprocess 
import re

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--interface", dest="interface", help="Interface to change its mode")
    options = parser.parse_args()
    if not options.interface:
        parser.error("[-] Please specify a wifi interface")
    return options

def change_mode(interface):

    output = str(subprocess.check_output(["iwconfig", interface]), "utf-8")
    managed = re.search('Managed', output)
    monitor = re.search('Monitor', output)

    if managed:
        print("[+] Changing mode of " + interface + " to Monitor")
        subprocess.call(["ifconfig", interface, "down"])
        subprocess.call(["iwconfig", interface, "mode", "monitor"])
        subprocess.call(["ifconfig", interface, "up"])
        print ("[+] Interface mode successfully changed to Monitor mode")

    if monitor:
        print("[+] Changing mode of " + interface + " to Managed")
        subprocess.call(["ifconfig", interface, "down"])
        subprocess.call(["iwconfig", interface, "mode", "managed"])
        subprocess.call(["ifconfig", interface, "up"])
        print ("[+] Interface mode successfully changed to Monitor mode")

    iwconfig_output = subprocess.check_output("iwconfig", interface)
    print (iwconfig_output)
    
options = get_arguments()
change_mode(options.interface)
    
