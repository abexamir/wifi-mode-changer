# Wifi Mode Switcher


Wifi interfaces work in two modes: Managed mode & Monitor mode.

Normally, interfaces are in Managed mode to connect to a wifi network. 

To use Wifi card to listen to other networks that it's not connected to them, The interface must bet in Monitor mode. 

This simple script does this switching. \n

**Usage:**
```bash
python3 wifimode.py -i [WiFi interface name]
```